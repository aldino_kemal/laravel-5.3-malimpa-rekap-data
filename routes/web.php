<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'HomeGetController@index');
Route::post('/', 'HomePostController@proces');
Route::get('/noscript', function(){
	return view('noscript');
});

Route::get('/anggota', 'HomeGetController@anggota');

Route::get('login', 'LoginController@login');
Route::post('login', 'LoginController@auth');


Route::group(['middleware' => 'login_validation'], function () {
	Route::get('export', 'HomeGetController@export');
	Route::get('anggota/edit/{anggota_id}', 'HomeGetController@edit_data');
	Route::post('anggota/edit/{anggota_id}', 'HomePostController@edit_data');

	Route::post('anggota/delete/{anggota_id}', 'HomePostController@delete_data');
});

Route::get('logout', function() {
    if (session()->has('malimpa')){
    	session()->flush();
    	return redirect('/anggota')->with(['berhasil_logout' => "Logout berhasil"]);	
    }
    return abort(404);
    
});