<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnggotaModel extends Model
{
    protected $table = 'anggota';
	protected $primaryKey = 'anggota_id';
	protected $fillable = [
		'anggota_id', 
	    'nama', 
	    'nama_lapangan',
	    'nia', 
	    'jabatan', 
	    'alamat', 
	    'no_hp',
	    'created_at', 
	    'updated_at'
	];
}
