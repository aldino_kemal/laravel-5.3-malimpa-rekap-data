<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class LoginModel extends Authenticatable
{
    use Notifiable;

    protected $table = 'logins';

    protected $primaryKey = 'username';

    protected $fillable = [
        'username', 'password', 'last_login', 'created_at', 'updated_at'
    ];

}
