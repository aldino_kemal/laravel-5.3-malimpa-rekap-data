<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\DataValidationRequests;
use App\AnggotaModel;

class HomePostController extends Controller
{
    public function proces(DataValidationRequests $request)
    {
    	try 
    	{
    		// $data_input = $request->all();
	    	// $new_anggota = new AnggotaModel;
	    	// $new_anggota->fill($data_input);
	    	// $new_anggota->save();


            $new_anggota = new AnggotaModel;
            $new_anggota->nama = strtoupper($request['nama']);
            $new_anggota->nama_lapangan = strtoupper($request['nama_lapangan']);
            $new_anggota->nia = strtoupper($request['nia']);
            $new_anggota->jabatan = strtoupper($request['jabatan']);
            $new_anggota->alamat = strtoupper($request['alamat']);
            $new_anggota->no_hp = $request['no_hp'];
            $new_anggota->save();

	    	return redirect()->back()->withSuccess($request['nama']);
    	}

    	catch(\Illuminate\Database\QueryException $e)
    	{
    		return redirect()->back()->withErrors(['duplicated' => $request['nama']]);	
    	}
    	
    }

    public function edit_data(DataValidationRequests $request, $anggota_id)
    {

        $update_anggota = AnggotaModel::findOrFail($anggota_id);
        $update_anggota->nama = strtoupper($request['nama']);
        $update_anggota->nama_lapangan = strtoupper($request['nama_lapangan']);
        $update_anggota->nia = strtoupper($request['nia']);
        $update_anggota->jabatan = strtoupper($request['jabatan']);
        $update_anggota->alamat = strtoupper($request['alamat']);
        $update_anggota->no_hp = $request['no_hp'];
        $update_anggota->update();

        return response()->json([
                'success'  => true,
                'data'     => 'berhasil'
            ], 200);
        
    }

    public function delete_data(Request $request, $anggota_id)
    {
        $delete_data = AnggotaModel::findOrFail($anggota_id);
        $delete_data->delete();
        return response()->json([
                'success'  => true,
                'data'     => 'berhasil'
            ], 200);
    }
}
