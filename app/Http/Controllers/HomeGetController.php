<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Excel;
use App\AnggotaModel;

class HomeGetController extends Controller
{
    public function index(Request $request)
    {	    	
    	return view('home')->with(['url' => $request->url()]);
    }

    public function anggota(Request $request)
    {
    	$datas = DB::table('anggota')->orderBy('created_at','desc')->get();
    	return view('anggota')->with(['datas' => $datas, 'url' => $request->url()]);
    }
    
    public function edit_data(Request $request, $anggota_id)
    {
        $datas = AnggotaModel::findOrFail($anggota_id);        
        return response()->json($datas);
    }

    public function export(Request $request)
    {
    	Excel::create('Daftar Anggota MALIMPA UMS', function($excel) {

        $excel->sheet('Anggota MALIMPA', function($sheet) {
            $sheet->setOrientation('landscape');
            // AKSES database
            $datas = DB::table('anggota')->orderBy('nia')->get();

            $isi_excel = [['Nama', 'Nama Lapangan', 'NIA', 'Jabatan', 'Alamat', 'Nomor HP']];

            $isi_sementara = [];
            foreach ($datas as $data) {
                array_push($isi_sementara, $data->nama);
                array_push($isi_sementara, $data->nama_lapangan);
                array_push($isi_sementara, $data->nia);
                array_push($isi_sementara, $data->jabatan);
                array_push($isi_sementara, $data->alamat);
                array_push($isi_sementara, $data->no_hp);
                array_push($isi_excel, $isi_sementara);
                $isi_sementara = [];
            }

            // dd($isi_sementara);
            // proses pemasukan (isi_excel,null,awal kolom, false, header range)
            $sheet->fromArray($isi_excel,null, 'A1', false, false);

            });
        })->download('xls');
    }
}
