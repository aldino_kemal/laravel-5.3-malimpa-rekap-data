<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LoginValidationRequests;
use DB;


class LoginController extends Controller
{
	public function login(Request $request)
	{

		return view('login')->with(['url' => $request->url()]);
	}

    public function auth(LoginValidationRequests $request)
    {
    	// dd($request->all());

    	$credential = [
            'username' => $request['username'],
            'password' => $request['password'],
        ];
    	
    	$auth = auth('malimpa');
    	if ($auth->attempt($credential)){
    		DB::table('logins')->update(['last_login' => date('Y-m-d H:i:s')]);    		
    		session(['malimpa' => $request->ip()]);
    		// dd(session('url'));
    		return redirect('/anggota')->with(['url' => $request->url()]);
    		

    	}
    	return redirect()->back()->withErrors(['username' => $request['username']])
    							 ->withInput($request->except('password'));
    }
}
