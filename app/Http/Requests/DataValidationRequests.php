<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DataValidationRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => 'required|string',
            'nama_lapangan' => 'string',
            'nia' => 'min:15|max:20',
            'jabatan' => 'required|string',
            'alamat' => 'required|string',
            'no_hp' => 'required|numeric',         
        ];
    }

    public function messages()
    {
        return[
            'nama.required' => 'Mohon isikan Nama Lengkap',
            'nama.string' => 'Nama hanya boleh berisi karakter',
            'nama_lapangan.alpha_num' => 'Nama Lapangan hanya dapat berisi karakter atau angka',
            'nia.min' => 'format NIA salah',
            'nia.max' => 'format NIA salah',            
            'jabatan.required' => 'Pilih posisi anda sewaktu di MALIMPA UMS',
            'alamat.required' => 'Mohon isikan Alamat',
            'alamat.alpha_num' => 'Alamat hanya dapat berisi karakter atau angka',
            'no_hp.required' => 'Mohon isikan Nomor HP',
            'no_hp.numeric' => 'Format nomor HP salah',
        ];
    }
}
