<?php

namespace App\Http\Middleware;

use Closure;

class LoginValidation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->session()->has('malimpa')) {
            return $next($request);
        }

        return redirect("/login");
    }
}
