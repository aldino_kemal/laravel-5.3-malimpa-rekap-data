@extends('layout.master')
@section('title', 'Pendataan Anggota Malimpa | MALIMPA UMS')
@section('meta_tag')
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
@endsection
@section('add_css')
    <style type="text/css">
        .malimpa_head {
            background: url('{{ asset('images/Logo_Malimpa_Warna.png') }}');
            background-repeat: no-repeat;
            background-position: center;
            padding-bottom: 200px;

        }
    </style>
@endsection

@section('content')
    @if(session()->has('success'))
        <script type="text/javascript">
            window.onload = function () {
                swal("Terimakasih", "Data {{ session('success') }} berhasil ditambah", "success");
            }            
        </script>
    @endif

    @if($errors->has('duplicated'))
        <script type="text/javascript">
            window.onload = function () {
                swal("Gagal", "Data {{ $errors->first('duplicated') }} telah ada", "error");
            }
        </script>
    @endif

    @if($errors->has('csrf_token'))
        <script type="text/javascript">
            window.onload = function () {
                swal("TokenMismatch", "{{ $errors->first('csrf_token') }}", "error");
            }
        </script>
    @endif
    <div class="container main" style="margin-top:10px">
        <div class="panel panel-default">
            <div class="panel-body">
                <p style="text-align: center; font-size:40px" id="header_daftar">PENDATAAN ANGGOTA</p>
                <div class="malimpa_head" id="malimpa_head">
                </div>
                <hr>
                <br><br>

                <div class="row">
                    <form class="col s12" method="post" id="forms">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="input-field col s12">
                                <i class="material-icons prefix">account_circle</i>
                                <input id="nama" name="nama" type="text" class="validate tooltipped" data-position="bottom" value="{{ old('nama') }}" data-delay="50" data-tooltip="Wajib diisi">
                                <label for="nama">Nama Lengkap</label>
                                @if ($errors->has('nama'))
                                    <div style="margin:-10px 0px 10px 50px">
                                        <strong style="color:#DD3333;"><i
                                                    class="material-icons">error</i> {{ $errors->first('nama') }}
                                        </strong>

                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <i class="material-icons prefix">face</i>
                                <input id="nama_lapangan" type="text" name="nama_lapangan" class="validate tooltipped" data-position="bottom" value="{{ old('nama_lapangan') }}" data-delay="50" data-tooltip="Kosongkan jika lupa">
                                <label for="nama_lapangan">Nama Lapangan</label>
                                @if ($errors->has('nama_lapangan'))
                                    <div style="margin:-10px 0px 10px 50px">
                                        <strong style="color:#DD3333;"><i
                                                    class="material-icons">error</i> {{ $errors->first('nama_lapangan') }}
                                        </strong>

                                    </div>
                                @endif                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <i class="material-icons prefix">book</i>
                                <input id="nia" name="nia" type="text" class="validate tooltipped" data-position="bottom" value="{{ old('nia') }}" data-delay="50" data-tooltip="NIA XX.XX.XXX MPA  (kosongkan jika lupa)">
                                <label for="nia">NIA</label>
                                @if ($errors->has('nia'))
                                    <div style="margin:-10px 0px 10px 50px">
                                        <strong style="color:#DD3333;"><i
                                                    class="material-icons">error</i> {{ $errors->first('nia') }}
                                        </strong>

                                    </div>
                                @endif                                
                            </div>
                        </div>
                        <!-- JABATAN BELUM BENER (Tidak ada name) -->
                        <div class="row">
                            <div class="input-field col s12">
                                <i class="material-icons prefix">work</i>
                                    <input type="text" name="jabatan" list="posisi" class="validate tooltipped" data-position="bottom" value="{{ old('jabatan') }}" data-delay="50" data-tooltip="Tuliskan sesuai posisi anda (wajib diisi)">
                                    <label for="jabatan">Jabatan</label>
                                    <datalist id="posisi">
                                        <option value="KETUA UMUM">
                                        <option value="BENDAHARA UMUM">
                                        <option value="SEKRETARIS UMUM">
                                        <option value="ANGGOTA">
                                    </datalist>                                    
                                @if ($errors->has('jabatan'))
                                    <div style="margin:-10px 0px 10px 50px">
                                        <strong style="color:#DD3333;"><i
                                                    class="material-icons">error</i> {{ $errors->first('jabatan') }}
                                        </strong>
                                    </div>
                                @endif                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <i class="material-icons prefix">home</i>
                                <input id="alamat" name="alamat" type="text" class="validate tooltipped" data-position="bottom" value="{{ old('alamat') }}" data-delay="50" data-tooltip="Wajib diisi">
                                <label for="alamat">Alamat</label>
                                @if ($errors->has('alamat'))
                                    <div style="margin:-10px 0px 10px 50px">
                                        <strong style="color:#DD3333;"><i
                                                    class="material-icons">error</i> {{ $errors->first('alamat') }}
                                        </strong>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <i class="material-icons prefix">call</i>
                                <input id="no_hp" type="text" name="no_hp" class="validate tooltipped" data-position="bottom" value="{{ old('no_hp') }}" data-delay="50" data-tooltip="Wajib diisi">
                                <label for="no_hp">Nomor HP</label>
                                @if ($errors->has('no_hp'))
                                    <div style="margin:-10px 0px 10px 50px">
                                        <strong style="color:#DD3333;"><i
                                                    class="material-icons">error</i> {{ $errors->first('no_hp') }}
                                        </strong>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div>
                            <div style="float: left">
                                <button class="btn waves-effect waves-light z-depth-3" type="button" name="action"
                                        onclick="send()">Tambah
                                    <i class="material-icons right">&#xE147;</i>
                                </button>
                                <div class="progress" id="progress_send" style="visibility: hidden">
                                    <div class="indeterminate"></div>
                                </div>
                            </div>
                            <div style="float: right;" id="lihat_data">
                                <a href="{{ url('anggota') }}" class="btn waves-effect waves-light z-depth-3"
                                   type="button" name="action" target="_blank">Lihat
                                    Data
                                    <i class="material-icons right">description</i>
                                </a>
                            </div>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>
@section('add_js')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.tooltipped').tooltip({delay: 50});
        });

        $(document).ready(function () {
            $('select').material_select();
        });

        function send() {
            $('#progress_send').attr('style', "visibility: visible");
            $('#forms').submit();
        }

        function info() {
            swal("Excel exported", "Save as :\n\"Daftar Hadir Musker 2016.xls\"")
        }
        if (window.matchMedia('screen and (max-width: 530px)').matches) {
            $('#malimpa_head').attr('style', 'background-size: 250px 100px;');
            $('#header_daftar').attr('style', 'font-size: 20px;text-align: center;');
            $('#lihat_data').attr('style', 'float: left; padding-top: 30px');
        }

    </script>
@endsection

@endsection
