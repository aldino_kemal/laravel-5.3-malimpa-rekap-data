<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <noscript><meta http-equiv="refresh" content="0; URL=/noscript" /></noscript>
    <title>@yield('title')</title>
    @yield('meta_tag')
    <link rel="shortcut icon" href="{{ asset('/images/Logo_Malimpa_Warna.png') }}">
    <!-- Bootstrapp CSS-->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('css/font-awesome.min.css') }}" rel='stylesheet' type='text/css'>
    <!-- SweetAlert CSS-->
    <link href="{{ asset('css/sweetalert.css') }}" rel="stylesheet">
    <!-- Material CSS -->
    <link href="{{ asset('css/materialize.min.css') }}" rel="stylesheet">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


    @yield('add_css')

<!-- JQuery -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <!-- Bootstrap JS -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- SweetAlert JS -->
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    <!-- Material JS -->
    <script src="{{ asset('js/materialize.min.js') }}"></script>
    
    @yield('head_add_js')

    

    <style type="text/css">
        @import 'https://fonts.googleapis.com/css?family=Raleway';

        body, h2, h5, p {
            /*font-size: 13px;*/
            font-family: 'Raleway', sans-serif;
        }
        @media all and (max-width: 768px) {
            #footer {
                width:100%;
                position:relative;
                bottom:0;
                left:0;
            }
        }
        @media all and (max-width: 1200px){
            #footer {
                width:100%;
                position:relative;
                bottom:0;
                left:0;
            }
        }
        
    </style>

</head>
<body>
@yield('content')

@if($url != url('/login'))
<div id="footer">
<footer class="page-footer teal darken-3">
    <div class="container ">
        <div class="row">
            <div class="col l6 s12">
                <h5 class="white-text">Untuk informasi lebih lengkap hubungi </h5>
                <p class="grey-text text-lighten-4"><i class="material-icons left">email</i>malimpaums@gmail.com</p>
                <p class="grey-text text-lighten-4"><i class="material-icons left">phone</i>
                <ol style="list-style-type:none; color: #FFFFFF">
                    <li>085728300240 Ajeng Nurtri H</li>
                    <li>085747159640 Maftufin Eqsan</li>                    
                </ol>
                </p>
            </div>
            <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Media Sosial</h5>
                <ul>                
                    <li><a class="grey-text text-lighten-3" href="https://www.facebook.com/MalimpaAdventure/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i>  &nbsp; Facebook</a></li>
                    <li><a class="grey-text text-lighten-3" href="https://www.instagram.com/malimpa" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i>&nbsp;&nbsp;Instagram</a></li>
                    <li><a class="grey-text text-lighten-3" href="https://twitter.com/malimpa" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i>&nbsp;&nbsp;Twitter</a></li>
                    
<!--                     <li><a class="grey-text text-lighten-3" href="#!"  target="_blank">Link 3</a></li>
                    <li><a class="grey-text text-lighten-3" href="#!"  target="_blank">Link 4</a></li> -->
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            Powered by <a class="grey-text text-lighten-4" href="http://www.fostiums.com">FOSTI UMS</a> © <script>
                        document.write(new Date().getFullYear())
                    </script> 
            <a class="grey-text text-lighten-4 right" href="http://malimpa.ukm.ums.ac.id/" target="_blank">MALIMPA UMS</a>
        </div>
    </div>
</footer>
</div>
@endif

<script>
    if (window.matchMedia('screen and (max-width: 530px)').matches) {
        $('#footer').attr('style', 'width:100%;position:relative;bottom:0;left:0;');
    }
</script>
@yield('add_js')
</body>

</html>
