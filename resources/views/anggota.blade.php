@extends('layout.master')

@section('title', 'Daftar Anggota Malimpa | MALIMPA UMS')

@section('meta_tag')
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('add_css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.material.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.0/material.min.css">

@endsection

@section('head_add_js')
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="{{ asset('js/jquery.datatables.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.material.min.js"></script>
    <script>
        $(document).ready(function () {
            data_table = $('#example').DataTable({
                responsive: false,
                lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
                columnDefs: [
                    {
                        targets: [0, 1, 2],
                        className: 'mdl-data-table__cell--non-numeric',
                        language: {
                            search: "_INPUT_",
                            searchPlaceholder: "Search records"
                        },
                    }
                ]
            });
        });
    </script>
@endsection

@section('content')
    @if (session()->has('berhasil_logout'))
        <script type="text/javascript">
            Materialize.toast('Logout berhasil', 5000, 'rounded toastposition');
        </script>
    @endif
    @if (session()->has('malimpa'))
        <script type="text/javascript">
            Materialize.toast('Anda dalam posisi login, jangan lupa logout', 5000, 'rounded toastposition');
        </script>
    @endif

    <nav>
        <div class="nav-wrapper teal darken-3">
            <a href="{{ url('/') }}" class="brand-logo center"><img src="{{ asset('/images/Logo_Malimpa_Warna.png') }}" style="height: 30%;width: 30%;" id="logo_malimpa"></a>
            <ul id="nav-mobile" class="ide-on-med-and-down ">
                <li>
                    @if(session()->has('malimpa'))
                        <button class="waves-effect waves-light btn-large teal darken-3"
                                style="padding-bottom: 63px" onclick="logout();" id="btn_logout">Logout
                        </button>
                    @else
                        <button class="waves-effect waves-light btn-large teal darken-3"
                                style="padding-bottom: 63px" onclick="login();" id="btn_login">Login
                        </button>
                    @endif
                </li>
            </ul>
        </div>
    </nav>

    <div class="container main" style="margin-top:10px">

        <h2 class="center-align">Data Anggota MALIMPA UMS</h2>

        <div class="table-responsive">
            <table id="example" class="mdl-data-table" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>No</th>
                    <th id="head_nama">Nama</th>
                    <th id="head_nama_lapangan">Nama Lapangan</th>
                    <th id="head_nia">NIA</th>
                    <th id="head_jabatan">Jabatan</th>
                    <th id="head_alamat">Alamat</th>
                    <th id="head_no_hp">Nomor HP</th>
                    <th id="head_no_hp">Tanggal</th>
                    @if (session()->has('malimpa'))
                        <th id="head_aksi">Aksi</th>
                    @endif
                </tr>
                </thead>
                <tbody>

                @foreach($datas as $data)
                    <tr id="data{{ $data->anggota_id }}">
                        <td>{{ $loop->iteration }} <!-- <span class="glyphicon glyphicon-chevron-down right" aria-hidden="true"></span> --></td>
                        <td>{{ $data->nama }}</td>
                        <td>{{ $data->nama_lapangan }}</td>
                        <td>{{ $data->nia }}</td>
                        <td>{{ $data->jabatan }}</td>
                        <td>{{ $data->alamat }}</td>
                        <td>{{ $data->no_hp }}</td>
                        <td>{{ $data->created_at }}</td>
                        @if (session()->has('malimpa'))
                        <td>
                            <a class="tooltipped modal-trigger waves-effect waves-teal buka-modal_edit" data-position="left" data-delay="20" data-tooltip="Edit" data-anggota="{{ $data->anggota_id }}"><i class="material-icons" style="color: #00695C;font-size: 20px">mode_edit</i></a>
                            <a class="tooltipped delete_permanen waves-effect waves-teal" data-position="right" data-delay="20" data-tooltip="Delete" data-anggota="{{ $data->anggota_id }}"><i class="material-icons" style="color: #00695C; font-size: 20px">delete_forever</i></i></a></td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div style="padding-bottom: 60px">
                <center>
                    @if(session()->has('malimpa'))
                        <a href="{{ url('/export') }}"
                           class="waves-effect waves-light teal darken-3 btn">EXPORT TO EXCEL</a>
                    @else
                        <a href="{{ url('/export') }}"
                           class="waves-effect waves-light teal darken-3 btn tooltipped" data-position="top"
                           data-delay="20" data-tooltip="Login dahulu">EXPORT TO EXCEL</a>
                </center>
                @endif
            </div>
        </div>

    </div>



<!-- Modal EDIT -->
  <!-- Modal Structure -->
    <div id="modal_edit" class="modal modal-fixed-footer">
        <div class="modal-content">
            <h4>Edit Data</h4>
            <div class="row">
                <div class="row">
                    <div class="input-field col s12">
                        <input id="update_nama_lengkap" type="text" class="validate" placeholder="...">
                        <label for="update_nama_lengkap">Nama Lengkap</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="update_nama_lapangan" type="text" class="validate" placeholder="...">
                        <label for="update_nama_lapangan">Nama Lapangan</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="update_nia" type="text" class="validate" placeholder="...">
                        <label for="update_nia">NIA</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input type="text" id="update_jabatan" list="posisi" class="validate" placeholder="...">
                        <label for="jabatan">Jabatan</label>
                        <datalist id="posisi">
                            <option value="KETUA UMUM">
                            <option value="BENDAHARA UMUM">
                            <option value="SEKRETARIS UMUM">
                            <option value="ANGGOTA">
                        </datalist>                         
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="update_alamat" type="text" class="validate" placeholder="...">
                        <label for="update_alamat">Alamat</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="update_no_hp" type="text" class="validate" placeholder="...">
                        <label for="update_no_hp">Nomor HP</label>
                    </div>
                </div>

            </div>
        </div>
        <div class="modal-footer">
            <button class="left btn modal-action modal-close waves-effect waves-green btn-flat">
            Close</button>
            <button class="btn modal-action modal-close waves-effect waves-green btn-flat right" onclick="update_data();">
            Update</button>
        </div>
    </div>
          

<!-- EndModal Edit -->


@section('add_js')
    @if (session()->has('malimpa'))
        <script src="{{ asset('js/materialize.min.js') }}"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('select').material_select();
            });

            $('.modal').modal({
                dismissible: true, 
                opacity: 0.8, 
                in_duration: 300, 
                out_duration: 200,                           
                }
            );

            $(document).on('click','.buka-modal_edit',function (e) {
                id = $(this).attr("data-anggota");
                $.get('/anggota/edit' + '/' + id, function (data) {
                    $('#update_nama_lengkap').val(data.nama);
                    $('#update_nama_lapangan').val(data.nama_lapangan);
                    $('#update_nia').val(data.nia);
                    $('#update_jabatan').val(data.jabatan);
                    $('#update_alamat').val(data.alamat);
                    $('#update_no_hp').val(data.no_hp);
                });
                $('#modal_edit').modal('open');
                
            });

            function update_data(){          
                var id_tr = 'data'+id;
                var update_nama_lengkap = $('#update_nama_lengkap').val();
                var update_nama_lapangan = $('#update_nama_lapangan').val();
                var update_nia = $('#update_nia').val();
                var update_jabatan = $('#update_jabatan').val();
                var update_alamat = $('#update_alamat').val();
                var update_no_hp = $('#update_no_hp').val();
            
                var formData = {
                        nama : update_nama_lengkap,
                        nama_lapangan : update_nama_lapangan,
                        nia : update_nia,
                        jabatan : update_jabatan,
                        alamat : update_alamat,
                        no_hp : update_no_hp, 
                    };
                
                if (update_nama_lengkap == "" || update_nama_lapangan == "" || update_nia == "" || update_jabatan == "" || update_alamat == "" || update_no_hp == "") {
                    swal("Failed", "All field can\'t be empty", "error");
                    return false;
                }
                
                $.ajax({
                    type: 'POST',
                    url: '/anggota/edit/' + id,
                    data: formData,
                    dataType: 'json',
                    headers: {'X-CSRF-TOKEN': _token},
                    success: function (data) {
                        console.log('berhasil');
                        $('#'+id_tr+' td:nth-child(2)').html(update_nama_lengkap.toUpperCase());
                        $('#'+id_tr+' td:nth-child(3)').html(update_nama_lapangan.toUpperCase());
                        $('#'+id_tr+' td:nth-child(4)').html(update_nia.toUpperCase());
                        $('#'+id_tr+' td:nth-child(5)').html(update_jabatan.toUpperCase());
                        $('#'+id_tr+' td:nth-child(6)').html(update_alamat.toUpperCase());
                        $('#'+id_tr+' td:nth-child(7)').html(update_no_hp);
                        swal("Success", update_nama_lengkap + " successfully updated", "success");
                    },
                    error: function (data) {
                        swal("Validation Message", "Mohon isikan kolom sesuai aturan", "error");
                    }

                });           
            }

            $(document).on('click','.delete_permanen',function (e) {
                var id = $(this).attr("data-anggota");
                var id_tr = 'data'+id;
                var nama = $('#'+id_tr+' td:nth-child(2)').html();

                var row = $(this).parents('tr');
                
                var formData = {
                    anggota_id : id
                };

                swal({
                    title: "Delete " + nama + " ?",
                    text: "Your action can\'t be undone",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function(){
                    $.ajax({
                    type: 'POST',
                    url: '/anggota/delete/' + id,
                    data: formData,
                    dataType: 'json',
                    headers: {'X-CSRF-TOKEN': _token},
                    success: function (data) {                   
                        data_table.row(row).remove().draw();
                        swal("Deleted!", nama + " successfully deleted", "success");
                    },
                    error: function (data) {
                        swal("Validation Message", "Mohon isikan kolom sesuai aturan", "error");
                    }
                });           
                    
                    
                });            
                
            });
        </script>
    @endif
    <script type="text/javascript">
        _token = $('meta[name="csrf-token"]').attr('content');

        function login() {
            window.location.href = "{{ url('/login') }}";
        }
        function logout() {
            window.location.href = "{{ url('/logout') }}";
        }
        if (window.matchMedia('screen and (max-width: 547px)').matches) {
            $('#logo_malimpa').attr('style', 'height: 60%;width: 60%');
            $('#btn_login').attr('style', 'padding-bottom: 50px');
            $('#btn_logout').attr('style', 'padding-bottom: 50px');
        }        
    </script>

@endsection


@endsection
