<?php

use Illuminate\Database\Seeder;

class InsertDataAnggotaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('anggota')->insert([
        	[	
	        	'nama' => 'IBNU SYARIFUDDIN',
	        	'nama_lapangan' => 'JAMBAN',
	        	'nia' => 'NIA 14.32.009 MPA',
	        	'jabatan' => 'BENDAHARA UMUM',
	        	'alamat' => 'LAMPUNG',
	        	'no_hp' => '09867890',
	        	'updated_at' => date('Y-m-d H:i:s'),
	        	'created_at' => date('Y-m-d H:i:s'),
        	],
        ]);
    }
}
