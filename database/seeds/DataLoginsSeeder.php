<?php

use Illuminate\Database\Seeder;

class DataLoginsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('logins')->insert([
        	[	
				'username' => 'malimpa',
        		'password' => bcrypt('malimpa'),
        		'last_login' => date('Y-m-d H:i:s'),
	        	'updated_at' => date('Y-m-d H:i:s'),
	        	'created_at' => date('Y-m-d H:i:s'),
        	],
        ]);
    }
    
}
